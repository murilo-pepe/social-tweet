# Introdução

Aplicação realizada como desafio técnico para o processo seletivo da Social Base.
Criado com micro-framework Lumen (lumen.laravel.com)

Como driver de cache foi utilizado "database" para desenvolvimento, recomenda-se 
mudar para redis ou memcached dependendo da disponibilidade dos serviços em produção

# Instalação

1. Instale as dependências através do seguinte comando no terminal:


```
#!cmd

composer install
```


2.Crie dois bancos de dados (nomes recomendado: social-tweet e social-tweet-tests)


```
#!sql

CREATE SCHEMA social-tweet DEFAULT CHARACTER SET utf8 ;
CREATE SCHEMA social-tweet-tests DEFAULT CHARACTER SET utf8 ;
```


3.Configure o arquivo .env com as configurações do banco criado para a aplicação

4.Configure o arquivo phpunit.xml com as configurações do banco de testes

5.Execute as migrações do banco de dados através do seguinte comando no terminal:


```
#!cmd

php artisan migrate
```


# Rodando os testes

Para executar os testes execute o seguinte comando no terminal:


```
#!python

phpunit
```

# Rotas

### GET tweet
Recupera toda a listagem de tweets

### GET tweet/{id}
Recupera um tweet baseado no id informado na rota

### POST tweet
Insere um novo tweet
Deve ser enviado um json com as chaves:
'tweet': entre 2 e 140 caracteres, obrigatório
'user': Entre 2 e 50 caracteres, apenas alphanumericos, _ ou .

### PUT tweet/{id}
Altera um tweet baseado no id informado na rota
Deve ser enviado um json com as chaves:
'tweet': entre 2 e 140 caracteres

### DELETE tweet/{id}
Deleta um tweet baseado no id informado na rota

### GET user/{user}/tweets
Recupera toda a listagem de tweets de um usuário
