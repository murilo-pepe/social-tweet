<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TweetController extends Controller {

    //Tempo de vida do cache em minutos 
    const CACHE_TTL = 2;

    private $tweetValidationRules = [
        'tweet' => 'required|max:140|min:2',
        //Usuários apenas alphanumericos, _ ou . exceto iniciando com . ou _ ou ./_ consecultivos (.. , __)
        'user' => ['required', 'max:50', 'min:2', 'regex:/^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/']
    ];

    public function index() {

        $tweets = Tweet::all();

        return response()->json($tweets);
    }

    public function getTweet($id) {

        $tweet = Cache::remember('tweet_' . $id, self::CACHE_TTL, function() use ($id) {

                    $tweet = Tweet::find($id);

                    if ($tweet === null) {
                        abort(404, 'Tweet não encontrado');
                    }

                    return $tweet;
                });

        return response()->json($tweet);
    }

    public function getTweetFromUser($user) {

        $tweets = Tweet::where('user', $user)->get();      
        
        
        return response()->json($tweets);
    }

    public function saveTweet(Request $request) {

        $this->validate($request, $this->tweetValidationRules);

        $tweet = Tweet::create($request->all());
        Cache::put('tweet_' . $tweet->id, $tweet, self::CACHE_TTL);

        return response()->json($tweet);
    }

    public function deleteTweet($id) {
        $tweet = Tweet::find($id);

        if ($tweet === null) {
            abort(404, 'Tweet não encontrado');
        }

        $tweet->delete();
        Cache::forget('tweet_' . $id);

        return response()->json('Tweet deletado com sucesso');
    }

    public function updateTweet(Request $request, $id) {

        $this->validate($request, $this->tweetValidationRules);

        $tweet = Tweet::find($id);


        if ($tweet === null) {
            abort(404, 'Tweet não encontrado');
        }

        //Recusa atualização de nome do usuário
        if ($request->input('user') !== null && $tweet->user !== $request->input('user')) {
            abort(403, 'Não é possível alterar o nome do usuário');
        }

        $tweet->tweet = $request->input('tweet');

        $tweet->save();
        Cache::put('tweet_' . $id, $tweet, self::CACHE_TTL);

        return response()->json($tweet);
    }

}
