<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('tweet', 'TweetController@index');

$app->get('tweet/{id}', 'TweetController@getTweet');

$app->get('user/{user}/tweets', 'TweetController@getTweetFromUser');

$app->post('tweet', 'TweetController@saveTweet');

$app->put('tweet/{id}', 'TweetController@updateTweet');

$app->delete('tweet/{id}', 'TweetController@deleteTweet');
