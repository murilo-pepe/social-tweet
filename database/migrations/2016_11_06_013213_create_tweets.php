<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweets extends Migration {

    public function up() {
        Schema::create('tweets', function(Blueprint $table) {
            $table->increments('id');
            $table->string('tweet',140);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('tweets');
    }

}
