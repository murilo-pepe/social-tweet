<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class TweetTest extends TestCase {

    //Trait para reinicializar a base de dados para cada teste
    use DatabaseMigrations;

    private $validTweet = [];

    public function __construct($name = null, array $data = array(), $dataName = '') {
        $this->validTweet = [
            'tweet' => str_pad('', 140, '-'),
            'user' => 'user_a'
        ];

        parent::__construct($name, $data, $dataName);
    }

    // Verifica a inserção de um tweet válido
    public function testInsertValidTweet() {

        $this
                ->post('/tweet', $this->validTweet)
                ->seeJsonContains(['id' => 1]);

        $this->seeInDatabase('tweets', ['tweet' => $this->validTweet['tweet']]);
    }

    //Verifica retorno de erro ao inserir um tweet vazio
    public function testNoTweetInsertion() {
        $tweet = $this->validTweet;
        $tweet['tweet'] = '';

        $response = $this->call('POST', '/tweet', $tweet);

        $this->assertEquals(422, $response->status());
    }

    //Verifica retorno de erro ao inserir um tweet sem usuário
    public function testNoUserInsertion() {
        $tweet = $this->validTweet;
        $tweet['user'] = '';

        $response = $this->call('POST', '/tweet', $tweet);

        $this->assertEquals(422, $response->status());
    }

    //Verifica retorno de erro ao inserir um tweet com user inválido
    public function testInvalidUserInsertion() {
        $tweet = $this->validTweet;
        $tweet['user'] = 'asd asd';

        $response = $this->call('POST', '/tweet', $tweet);

        $this->assertEquals(422, $response->status());
    }

    //Verifica o retorno de erro ao inserir um tweet acima de 140 caracteres    
    public function testLargeTweetInsertion() {
        $tweet = $this->validTweet;
        $tweet['tweet'] = str_pad('', 141, '-');

        $response = $this->call('POST', '/tweet', $tweet);

        $this->assertEquals(422, $response->status());
    }

    //Verifica o retorno de erro ao inserir um tweet abaixo de 2 caracteres    
    public function testSmallTweetInsertion() {
        $tweet = $this->validTweet;
        $tweet['tweet'] = 'a';

        $response = $this->call('POST', '/tweet', $tweet);

        $this->assertEquals(422, $response->status());
    }

    //Verifica coleta de todos os tweets
    public function testGetAllTweets() {

        //Cria 20 tweets através do modelo definido em database/factories/ModelFactory.php
        factory('App\Tweet', 20)->create();

        $response = $this->call('GET', '/tweet');
        $this->assertEquals(200, $response->status());

        $responseContent = json_decode($response->content(), true);
        $this->assertCount(20, $responseContent);
    }

    
    //Verifica captura de tweets de um usuário específico
    public function testGetTweetsFromUser() {
        factory('App\Tweet', 10)->create(['user' => 'user_a']);
        factory('App\Tweet', 10)->create(['user' => 'user_b']);

        $response = $this->call('GET', '/user/user_a/tweets');
        $this->assertEquals(200, $response->status());

        $tweets = json_decode($response->content(), true);
        
        $this->assertCount(10, $tweets);
        foreach ($tweets as $tweet) {
            $this->assertEquals($tweet['user'], 'user_a');
        }
    }

    //Verifica consulta de um tweet específico
    public function testGetOneTweet() {

        //Cria 20 tweets através do modelo definido em database/factories/ModelFactory.php
        factory('App\Tweet', 20)->create();

        $response = $this->call('GET', '/tweet/10');
        $this->assertEquals(200, $response->status());

        $responseContent = json_decode($response->content(), true);
        $this->assertEquals($responseContent['id'], 10);
    }

    //Verifica a atualização de um tweet
    public function testUpdateTweet() {

        //Cria 20 tweets através do modelo definido em database/factories/ModelFactory.php
        factory('App\Tweet', 20)->create();

        //Coleta o tweet nº 10
        $getResponse = $this->call('GET', '/tweet/10');
        $this->assertEquals(200, $getResponse->status(), 'Falha ao recuperar tweet 10');

        $tweet = json_decode($getResponse->content(), true);
        $this->assertEquals($tweet['id'], 10);

        $tweet['tweet'] = 'Novo Tweet';

        //Atualiza o tweet nº10
        $putResponse = $this->call('PUT', '/tweet/10', $tweet);
        $this->assertEquals(200, $putResponse->status(), 'Falha ao atualizar tweet 10');

        //Recupera o tweet nº10 após atualização
        $getResponseUpdated = $this->call('GET', '/tweet/10');
        $this->assertEquals(200, $getResponseUpdated->status(), 'Falha ao recuperar tweet 10 após atualização');

        //Vefifica se foi atualizado com sucesso
        $updatedTweet = json_decode($getResponseUpdated->content(), true);
        $this->assertEquals($updatedTweet['id'], 10, 'Tweet errado');
        $this->assertEquals($updatedTweet['tweet'], 'Novo Tweet', 'Tweet não atualizado');
    }

    //Verifica a atualização de um tweet
    public function testUpdateTweetUser() {

        //Cria 20 tweets através do modelo definido em database/factories/ModelFactory.php
        factory('App\Tweet', 20)->create();

        //Coleta o tweet nº 10
        $getResponse = $this->call('GET', '/tweet/10');
        $this->assertEquals(200, $getResponse->status(), 'Falha ao recuperar tweet 10');

        $tweet = json_decode($getResponse->content(), true);
        $this->assertEquals($tweet['id'], 10);

        $tweet['user'] = 'user_novo';

        //Atualiza o tweet nº10
        $putResponse = $this->call('PUT', '/tweet/10', $tweet);
        $this->assertEquals(403, $putResponse->status(), 'Tweet atualizou usuário');
    }

    //Verifica deleção de um tweet
    public function testDeleteTweet() {
        factory('App\Tweet', 20)->create();

        //Coleta o tweet nº 10
        $getResponse = $this->call('GET', '/tweet/10');
        $this->assertEquals(200, $getResponse->status());

        //Deleta o tweet nº10
        $delResponse = $this->call('DELETE', '/tweet/10');
        $this->assertEquals(200, $delResponse->status());


        //Tenta coletar o tweet nº 10 novamente
        $getResponseDeleted = $this->call('GET', '/tweet/10');
        $this->assertEquals(404, $getResponseDeleted->status());
    }

}
